package controller;

import java.io.IOException;

import api.IDivvyTripsManager;
import model.logic.DivvyTripsManager;


public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	


	
	public static void loadStations(String pStationsFile) {
		manager.loadStations(pStationsFile);
	}
	

	public static void loadIntersecciones(String pIntereseccionesFile){
		manager.loadIntersecciones(pIntereseccionesFile);
	}

	
	public static void loadArcosMallaVial(String pIntereseccionesFile){
		manager.loadArcosMallaVial(pIntereseccionesFile);
	}
	
	
	public static void escribirTotalesGrafoInicial (String pRuta)
	{
		manager.escribirTotalesGrafoInicial(pRuta);
	}
	

	public static void loadStationsInterseccion()
	{
		manager.loadStationsInterseccion();
	}
	
	public static void escribirTotalesGrafoExtendido (String pRuta)
	{
		manager.escribirTotalesGrafoExtendido(pRuta);
	}
	
	public static void crearJson (String pRuta)
	{
		manager.crearJson(pRuta);
	}
	
	public static void cargarJson (String pRuta)
	{
		try {
			manager.cargarJson(pRuta);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
