package view;

import java.util.Scanner;

import controller.Controller;


public class DivvyTripsManagerView 
{
	//Ruta del archivo de datos 2017-Q1
		// TODO Actualizar
		public static final String TRIPS_Q1 = "./Data/Divvy_Trips_2017_Q1.csv";

		//Ruta del archivo de trips 2017-Q2
		// TODO Actualizar	
		public static final String TRIPS_Q2 = "./Data/Divvy_Trips_2017_Q2.csv";

		//Ruta del archivo de trips 2017-Q3
		// TODO Actualizar	
		public static final String TRIPS_Q3 = "./Data/Divvy_Trips_2017_Q3.csv";

		//Ruta del archivo de trips 2017-Q4
		// TODO Actualizar	
		public static final String TRIPS_Q4 = "./Data/Divvy_Trips_2017_Q4.csv";

		//Ruta del archivo de stations 2017-Q1-Q2
		// TODO Actualizar	
		public static final String STATIONS_Q1_Q2 = "./Data/Divvy_Stations_2017_Q1Q2.csv";

		//Ruta del archivo de stations 2017-Q3-Q4
		// TODO Actualizar	
		public static final String STATIONS_Q3_Q4 = "./Data/Divvy_Stations_2017_Q3Q4.csv";
		
		
		public static final String INTERSECCIONES = "./Data/Nodes_of_Chicago_Street_Lines.txt";
		
		public static final String ARCOS = "./Data/Adjacency_list_of_Chicago_Street_Lines.txt";
		
		public static final String LEAME = "./Data/Leame.txt";
		
		public static final String JSON = "./Data/Json.json";


		public static void main(String[] args) 
		{
			Scanner linea = new Scanner(System.in);
			boolean fin=false;
			while(!fin)
			{
				printMenu();

				int option = linea.nextInt();

				switch(option)
				{

				case 1:
					Controller.loadIntersecciones(INTERSECCIONES);
					Controller.loadArcosMallaVial(ARCOS);
					break;

				case 2:	
					Controller.escribirTotalesGrafoInicial(LEAME);
					break;	
					
				case 3:	
					Controller.loadStations(STATIONS_Q3_Q4);
					Controller.loadStationsInterseccion();
					break;	
					
				case 4:	
					Controller.escribirTotalesGrafoExtendido(LEAME);
					break;

				case 5:	
					Controller.crearJson(JSON);
					break;
					
				case 6:	
					Controller.cargarJson(JSON);
					break;
					
				case 7:	
					fin=true;
					linea.close();
					break;
				}
			}
		}

		private static void printMenu() {
			System.out.println("---------ISIS 1206 - Estructuras de datos----------");
			System.out.println("---------------------Taller 3----------------------");
			System.out.println("1. Crear grafo inicial malla vial");
			System.out.println("2. Escribir numero de vertices y arcos del grafo inicial en el Leame.txt en Data");
			System.out.println("3. Agregar estaciones al grafo");
			System.out.println("4. Escribir numero de vertices y arcos del grafo extendido en el Leame.txt en Data");
			System.out.println("5. Crear Json con grafo");
			System.out.println("6. Cargar grafo a partir de Json");
			System.out.println("7. Salir");
			System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

		}

}
