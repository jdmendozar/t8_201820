package model.vo;

import java.time.LocalDateTime;
import java.util.Date;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private String cityName; 
	private double latitude; 
	private double longitude; 
	private int dpCapacity;
	private LocalDateTime startDate;
	//TODO Completar

	public Station(int stationId, String stationName, String cityName, double latitude, double longitude, int dpCapacity, LocalDateTime startDate) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.cityName = cityName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpCapacity = dpCapacity;
		this.startDate = startDate;
	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
	
	public String getCityName() {
		return cityName;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public double getDPCapacity()
	{
		return dpCapacity;
	}
}
