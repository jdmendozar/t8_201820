package model.vo;

public class ArcoJson {
	int idSalida;
	int idLlegada;
	double peso;
	
	public ArcoJson(int nSal, int nLleg, double pPeso)
    {
    	peso = pPeso;
    	idSalida = nSal;
    	idLlegada = nLleg;
    }
	
	public double getPeso()
	{
		return peso;
	}
	
	public void setPeso(int p)
	{
		this.peso = p;
	}
	
	public int getNodoSalida()
	{
		return idSalida;
	}
	
	public int getNodoLlegada()
	{
		return idLlegada;
	}
	
	
}
