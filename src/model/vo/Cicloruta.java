package model.vo;

import java.util.ArrayList;

public class Cicloruta implements Comparable<Cicloruta> {

	private String id;
	private String tipo;
	private ArrayList<String> recorrido;
	private String calleReferencia;
	private String calleLimiteExtremo1;
	private String calleLimiteExtremo2;
	
	private String [] geo;
	
	private Double[] latitudes;
	
	private Double[] longitudes;
	
	private double distancia;
	
	
	public Cicloruta(String pId, String pTipo, String pCalleRef, String pCalleLim1, String[] pGeo, String pCalleLim2, double pDist)
	{
		id = pId;
		tipo = pTipo;
		
		calleReferencia = pCalleRef;
		calleLimiteExtremo1 = pCalleLim1;
		calleLimiteExtremo2 = pCalleLim2;
		geo= pGeo;
		
		asignarLatLong(geo);
		distancia = pDist;
	}

	public String getId()
	{
		return id;
	}
	
	public void asignarLatLong(String[] pCoordenadas)
	{
		Double[] lat = new Double[pCoordenadas.length];
		
		Double[]lon = new Double[pCoordenadas.length];
		
		for (int i = 0; i < pCoordenadas.length; i++) 
		{
			lon[i]=Double.parseDouble(pCoordenadas[i].split(" ")[0]);
			lat[i]=Double.parseDouble(pCoordenadas[i].split(" ")[1]);
		}
		
		
	}
	
	@Override
	public int compareTo(Cicloruta o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	public double getMayorLongitud()
	{
		double mayor=Double.MIN_VALUE;
		for(int i=0; i<longitudes.length ;i++)
		{
			if(longitudes[i]>mayor)
			{
				mayor=longitudes[i];
			}
		}
		return mayor;
	}
	
	public double getMayorLatitud()
	{
		double mayor=Double.MIN_VALUE;
		for(int i=0; i<latitudes.length ;i++)
		{
			if(latitudes[i]>mayor)
			{
				mayor=latitudes[i];
			}
		}
		return mayor;
	}
	
	public double getMenorLongitud()
	{
		double menor= Double.MAX_VALUE;
		for(int i=0; i<longitudes.length ;i++)
		{
			if(longitudes[i]<menor)
			{
				menor=longitudes[i];
			}
		}
		return menor;
	}
	
	public double getMenorLatitud()
	{
		double menor=Double.MAX_VALUE;
		for(int i=0; i<longitudes.length ;i++)
		{
			if(longitudes[i]<menor)
			{
				menor=longitudes[i];
			}
		}
		return menor;
	}

	}
