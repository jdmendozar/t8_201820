package model.vo;

public class Interseccion {

	public static final String ESTACION = "Estacion";
	public static final String INTERSECCION = "Interseccion";
	
	
	private int id;
	private double latitud;
	private double longitud;
	private String tipo;
	
	
	public Interseccion(int pId, double pLatitud, double pLongitud, String pTipo)
	{
		this.id = pId;
		this.latitud = pLatitud;
		this.longitud = pLongitud;
		this.tipo = pTipo;
	}
	
	
	public int darId(){
		return this.id;
	}
	
	public double darLatitud(){
		return this.latitud;
	}
	
	public double darLongitud(){
		return this.longitud;
	}
	
	public String darTipo(){
		return this.tipo;
	}
	
	
	
}
