package model.vo;

import java.util.List;

public class VOSector 
{
	private List<Cicloruta> cicloRutasPorSector;

	private Double longitudMinima;

	private Double longitudMaxima;

	private Double latitudMinima;

	private Double latitudMaxima;

	public VOSector(Double pLongMin, Double pLatMin, Double pLongMax, Double pLatMax)
	{
		longitudMinima= pLongMin;

		latitudMinima = pLatMin;

		longitudMaxima = pLongMax;

		latitudMaxima = pLatMax;
	}
	
	
	public boolean perteneceAlSector(Double pLongitud, Double pLatitud)
	{
		boolean resp= false;
		
		if(pLongitud>= longitudMinima && pLatitud>= latitudMinima)
		{
			if(pLongitud<= latitudMaxima && pLatitud<= latitudMaxima)
			{
				resp = true;
			}
		}
		
		return resp;
	}
	
	public void agregarCicloRuta (Cicloruta pCicloruta)
	{
		cicloRutasPorSector.add(pCicloruta);
	}
}


