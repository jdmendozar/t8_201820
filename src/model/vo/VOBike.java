package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike implements Comparable<VOBike> {

	private int bikeId;
	private int totalTrips;
	private int totalDistance;
	private int totalDuration;

	public VOBike(int bikeId, int totalTrips, int totalDistance, int ptotalDuration) {
		this.bikeId = bikeId;
		this.totalTrips = totalTrips;
		this.totalDistance = totalDistance;
		this.totalDuration = ptotalDuration;
	}

	@Override
	public int compareTo(VOBike o) {
		// TODO completar
		if(this.getTotalTrips()>0)
		{
			if(this.getTotalTrips()>o.getTotalTrips())
			{
				return 1;
			}
			else if(this.getTotalTrips()<o.getTotalTrips())
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}else
		{
			if(this.getTotalDistance()>o.getTotalDistance())
			{
				return 1;
			}else if(this.getTotalDistance()<o.getTotalDistance())
			{
				return -1;
			}
		}
		
		return 0;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTotalTrips() {
		return totalTrips;
	}
	
	public void sumTrips(int numberTrips)
	{
		totalTrips += numberTrips;
	}

	public int getTotalDistance() {
		return totalDistance;
	}
	
	public void sumTotalDistance(int numberDistance)
	{
		totalDistance += numberDistance;
	}
	
	public int getTotalDuration() {
		return totalDuration;
	}
	
	public void sumTotalDuration(int numberDuration)
	{
		totalDuration += numberDuration;
	}
}
