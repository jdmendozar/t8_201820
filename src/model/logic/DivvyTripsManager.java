package model.logic;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.data_structures.Arco;
import model.data_structures.Grafo;
import model.data_structures.Lista;
import model.data_structures.NodoVertice;
import model.data_structures.RedBlackBST;
import model.vo.ArcoJson;
import model.vo.Interseccion;
import model.vo.Station;
import model.vo.VOBike;
import model.vo.VOTrip;


public class DivvyTripsManager implements IDivvyTripsManager {

	public static final String TRIPS_Q4 = "./Data/Divvy_Trips_2017_Q4.csv";
	public static final String STATIONS_Q3_Q4 = "./Data/Divvy_Stations_2017_Q3Q4.csv";
	private static final int EARTH_RADIUS = 6371;
	
	private RedBlackBST<VOBike, Integer> bikes;
	private Lista<VOTrip> trips;
	private Lista<Station> stations;
	private Grafo<Interseccion, Integer> grafoIntersecciones;
	
	public void loadBikes()
	{
		bikes = new RedBlackBST<>();
		int contador = 0;
		
		Iterator<VOTrip> iter = trips.iterator();
		while(iter.hasNext())
		{
			VOTrip actual = iter.next();
			
			Station startStation = searchStartStation(actual);

			Station endStation = searchEndStation(actual);

			double lat1 = startStation.getLatitude();

			double long1 = startStation.getLongitude();

			double lat2 = endStation.getLatitude();

			double long2 = endStation.getLongitude();

			double distancia = distance(lat1, long1, lat2, long2);

			VOBike buscado = null;
			Iterator<Integer> iter2 = bikes.keys();

			while(iter2.hasNext()) 
			{
				int actualB = iter2.next();
				if(bikes.get(actualB).getBikeId() == actual.getBikeId())
				{
					buscado = bikes.get(actualB);

					buscado.sumTotalDistance((int)distancia);

					buscado.sumTotalDuration(actual.getTripDuration());

					buscado.sumTrips(1);
				}
			}

			if(buscado== null)
			{
				buscado = new VOBike(actual.getBikeId(), 1,(int) distancia, actual.getTripDuration());

				bikes.put(buscado.getBikeId(), buscado);
				contador++;
			}
		}
		
		
		System.out.println("El total de nodos es:" + contador);
		System.out.println("La altura real del arbol es:" + bikes.height());
		System.out.println("El promedio de altura es:" +  promedioAltura());
	}
	
	
	
	
	public VOBike get(Integer key)
	{
		return bikes.get(key); 
	}
	

	
	
	public Lista<Integer> buscarIdsEnRango(int Idmenor, int IdMayor)
	{
		Iterator<Integer> iter = bikes.KeysInRange(Idmenor, IdMayor);
		Lista<Integer> iDsEnRango = new Lista<>();
		
		while(iter.hasNext())
		{
			int actual = iter.next();
			iDsEnRango.add(actual);
		}
		return iDsEnRango;
	}
	
	
	public double promedioAltura()
	{
		double promedio=0;
		double sumaAlturas = 0;
		double contador = 0;
		
		Iterator<Integer> iter = bikes.keys();
		
		while(iter.hasNext())
		{
			int actual = iter.next();
			sumaAlturas = sumaAlturas + bikes.getHeight(actual);
			contador++;
		}
		
		
		promedio = sumaAlturas/contador;
	
		return promedio;
	}
	

	
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		trips = new Lista<>();
		File file = new File(tripsFile);
		
		try
		{
			CSVReader reader = new CSVReader (new FileReader(file));
			reader.readNext();
			String[] nextline;
			
			while((nextline=reader.readNext()) != null)
			{
				if(nextline != null)
				{
					int id = Integer.parseInt(nextline[0]);

					String strStartT = nextline[1];
					String strEndT = nextline[2];

					DateTimeFormatter formatter ;
					if (tripsFile ==  TRIPS_Q4)
					{
						formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:mm");
					}else
					{
						formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:mm:ss");	
					}
					LocalDateTime startT = LocalDateTime.parse(strStartT, formatter);

					LocalDateTime endT= LocalDateTime.parse(strEndT, formatter);

					int bikeID = Integer.parseInt(nextline[3]);

					int duratioN = Integer.parseInt(nextline[4]);

					int fromId = Integer.parseInt(nextline[5]);

					int toId = Integer.parseInt(nextline[7]);

					String gendeR = null;
					if(nextline.length>10)
					{
						gendeR = nextline[10];
					}

					VOTrip nuevo = new VOTrip(id, startT, endT, bikeID, duratioN, fromId, toId, gendeR);
					trips.addAtK(0,nuevo);		
				}
			}
			reader.close();
		}
		catch ( IOException e ) 
		{
			e.printStackTrace();
		}
	}
	
	
	public void loadStations (String pStationsFile)
	{
		stations = new Lista<>();
		File file2 = new File(pStationsFile);
		try {
			CSVReader reader = new CSVReader (new FileReader(file2));
			reader.readNext();
			String[] nextline;
			while((nextline=reader.readNext()) != null)
			{
				if(nextline != null)
				{

					int id = Integer.parseInt(nextline[0]);

					String nombre = nextline[1];

					String nombreCiudad = nextline[2];

					Double latitud = Double.parseDouble(nextline[3]);

					Double longitud = Double.parseDouble(nextline[4]);

					int dpc = Integer.parseInt(nextline[5]);

					String startDate = nextline[6];

					DateTimeFormatter formatter;
					if (pStationsFile ==  STATIONS_Q3_Q4)
					{
						formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:mm", Locale.ENGLISH);
					}else
					{
						formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:mm:ss", Locale.ENGLISH);	
					}

					LocalDateTime localDate = LocalDateTime.parse(startDate,formatter);

					Station nuevo = new Station(id, nombre, nombreCiudad, latitud, longitud, dpc, localDate);

					stations.addAtK(0, nuevo);


					nextline = null;
				}
			}
			reader.close();
		} catch ( IOException e ) 
		{
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	public Station searchStartStation(VOTrip trip)
	{
		Station actual = null;
		for (int i = 0; i < stations.size() && actual == null; i++) 
		{
			if(stations.get(i).getStationId() == trip.getStartStationId())
			{
				actual = stations.get(i);
			}
		}

		return actual;
	}

	public Station searchEndStation(VOTrip trip)
	{
		Station actual = null;
		for (int i = 0; i < stations.size() && actual == null; i++) 
		{
			if(stations.get(i).getStationId() == trip.getEndStationId())
			{
				actual = stations.get(i);
			}
		}
		return actual;
	}
	
	
	public static double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}

	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}


	
	
	
	public void loadIntersecciones(String pIntereseccionesFile)
	{
		File file = new File(pIntereseccionesFile);
		FileReader fileR = null;
		BufferedReader file2 = null;
		grafoIntersecciones = new Grafo<>();

		try 
		{
		    fileR = new FileReader(file);
		    file2 = new BufferedReader(fileR);
		} 
		catch (FileNotFoundException e) 
		{
		    System.out.println("No se encontro el archivo "+file.getName());
		}

		try 
		{
		    String linea = file2.readLine();
		    while( ( linea != null) )
		    {        
		    	String lineaSep[] = linea.split(",");
		    	int id = Integer.parseInt(lineaSep[0]);
		    	double latitud = Double.parseDouble(lineaSep[1]);
		    	double longitud = Double.parseDouble(lineaSep[2]);
		    	
		    	Interseccion nueva = new Interseccion(id, latitud, longitud, Interseccion.INTERSECCION);
		    	grafoIntersecciones.addVertex(nueva.darId(), nueva);
		    	
		        linea = file2.readLine();
		    }
		} 
		catch (IOException e) 
		{
		    e.printStackTrace();
		}
	}


	
	public void loadArcosMallaVial(String pIntereseccionesFile)
	{
		File file = new File(pIntereseccionesFile);
		FileReader fileR = null;
		BufferedReader file2 = null;


		try 
		{
		    fileR = new FileReader(file);
		    file2 = new BufferedReader(fileR);
		} 
		catch (FileNotFoundException e) 
		{
		    System.out.println("No se encontro el archivo "+file.getName());
		}

		try 
		{
		    String linea = file2.readLine();
		    while( ( linea != null) )
		    {        
		    	if(linea.startsWith("#"))
		    	{
		    		linea = file2.readLine();
		    	}
		    	else
		    	{

		    		String lineaSep[] = linea.split(" ");
			    	int idVertexIni = Integer.parseInt(lineaSep[0]);
			    	for(int i=1; i<lineaSep.length; i++)
			    	{
			    		int idVertexFin = Integer.parseInt(lineaSep[i]);
			    		Interseccion intersIni = grafoIntersecciones.getInfoVertex(idVertexIni);
			    		Interseccion intersFin = grafoIntersecciones.getInfoVertex(idVertexFin);
			    		
			    		double dist = distance(intersIni.darLatitud(), intersIni.darLongitud(), intersFin.darLatitud(), intersFin.darLongitud());
			    		
			    		
			    		grafoIntersecciones.addEdge(idVertexIni, idVertexFin, dist);
			    		
			    		
			    	}

			        linea = file2.readLine();
		    	}
		    }
		} 
		catch (IOException e) 
		{
		    e.printStackTrace();
		}
	}
	
	
	
	public int darNumVertices()
	{
		int resp=0;
		resp= grafoIntersecciones.V();
		return resp;
	}
	
	
	
	public int darNumArcos()
	{
		int resp=0;
		resp= grafoIntersecciones.E();
		return resp;
	}
	
	
	
	public void escribirTotalesGrafoInicial (String pRuta)
	{
		
		String contenido = "En el grafo inicial el total de vertices es: " + darNumVertices() +", y el total de arcos es: " + darNumArcos() + ".";
		File file = new File(pRuta);
		try {
			PrintWriter escritor = new PrintWriter(file);
			escritor.println(contenido);
			escritor.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public void loadStationsInterseccion ()
	{
		Iterator<Station> iter = stations.iterator();
		
		while(iter.hasNext())
		{
			Station actual = iter.next();
			
			int id = grafoIntersecciones.V() + actual.getStationId();
			double lat = actual.getLatitude();
			double longi = actual.getLongitude();
			Interseccion masCercana = null;
			double distMasCercana = Double.MAX_VALUE;
			
			Interseccion nueva = new Interseccion(id, lat, longi, Interseccion.ESTACION);
			grafoIntersecciones.addVertex(id, nueva);
			
			
			Iterator<Interseccion> iter2 = grafoIntersecciones.darGrafo().values();
			while(iter2.hasNext())
			{
				Interseccion actual2 = iter2.next();
				if(actual2.darTipo().equals(Interseccion.INTERSECCION))
				{
					double dist = distance(nueva.darLatitud(), nueva.darLongitud(), actual2.darLatitud(), actual2.darLongitud());
					
					if(dist < distMasCercana)
					{
						masCercana = actual2;
						distMasCercana = dist;
					}	
				}
			}
			grafoIntersecciones.addEdge(nueva.darId(), masCercana.darId(), distMasCercana);
		}
	}
	
	public int darNumVerticesEstacion()
	{
		int resp=0;
		Iterator<Interseccion> iter = grafoIntersecciones.darGrafo().values();
		while(iter.hasNext())
		{
			Interseccion actual = iter.next();
			
			if(actual.darTipo().equals(Interseccion.ESTACION))
			{
				resp++;
			}
	
		}
		return resp;
	}
	
	
	
	@SuppressWarnings("rawtypes")
	public int darNumArcosMixtos()
	{
		int resp=0;
		Iterator<Interseccion> iter = grafoIntersecciones.darGrafo().values();
		Iterator<NodoVertice> iter1 = grafoIntersecciones.darGrafo().nodes();
		
		while(iter.hasNext())
		{
			Interseccion actualI = iter.next();
			NodoVertice actualN = iter1.next();
			
			
			if(actualI.darTipo().equals(Interseccion.ESTACION))
			{
				resp += actualN.getListaArcos().size();
			}
	
		}
		return resp;
	}
	
	
	
	
	public void escribirTotalesGrafoExtendido (String pRuta)
	{
		
		String contenido = "En el grafo extendido el total de vertices estacion es: " + darNumVerticesEstacion() +", y el total de arcos mixtos es: " + darNumArcosMixtos() + ".";
		File file = new File(pRuta);
		try {
			PrintWriter escritor = new PrintWriter(file);
			escritor.println(contenido);
			escritor.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void crearJson(String pRuta)
	{
		Gson gson = new Gson();
		FileWriter writer;
		try {
			writer = new FileWriter(pRuta);
			
			Iterator<Interseccion> iter = grafoIntersecciones.darGrafo().values();
			
			Interseccion actual = iter.next();
			writer.write("[");
			gson.toJson(actual,writer);
			while(iter.hasNext())
			{
				writer.write(",");
				actual = iter.next();
		        //2. Convert object to JSON string and save into a file directly

				gson.toJson(actual, writer);
				
			}
			
			Iterator<NodoVertice> iter2 = grafoIntersecciones.darGrafo().nodes();
			
			while(iter2.hasNext())
			{
				NodoVertice actual2 = iter2.next();
				
				
				Iterator<Integer> iter3 = actual2.getListaArcos().iterator();
				while(iter3.hasNext())
				{
					writer.write(",");
					int actual3 = iter3.next();
					Arco arc = (Arco) actual2.getListaArcos().searchObject(actual3);
					ArcoJson nuevo = new ArcoJson( (int) arc.getidSalida(),(int) arc.getidLlegada(), arc.getPeso());
					gson.toJson(nuevo, writer);		
				}
			}
			
			writer.write("]");
	
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	
	@SuppressWarnings("resource")
	public void cargarJson(String tripsFile) throws IOException
	{
		grafoIntersecciones = new Grafo<>();

		JsonReader reader = new JsonReader(new FileReader(tripsFile));

		
		reader.beginArray();
		
		while (reader.hasNext()) 
			{
			
			
			int id =-1;
			double latitud=0;
			double longitud=0;
			String tipo="";
			int idSalida=-1;
			int idLlegada = 0;
			double peso = 0;
			
	
			
				reader.beginObject();
					
					while (reader.hasNext()) 
					{
		
						String name = reader.nextName();
						if (name.equals("id")) 
						{
							id = reader.nextInt();
						} 
						else if (name.equals("latitud")) 
						{
							latitud = reader.nextDouble(); 
						} 
						else if (name.equals("longitud")) 
						{
							longitud = reader.nextDouble();
						}
						else if (name.equals("tipo")) 
						{
							tipo = reader.nextString();
						}
						else if (name.equals("idSalida")) 
						{
							idSalida = reader.nextInt();
						}
						else if (name.equals("idLlegada")) 
						{
							idLlegada = reader.nextInt();
						}
						else if (name.equals("peso")) 
						{
							peso = reader.nextDouble();
						}
						else 
						{
							reader.skipValue();
						}
			
					}
				reader.endObject();
				if(id!=(-1))
				{
					Interseccion nueva = new Interseccion(id, latitud, longitud, tipo);
					grafoIntersecciones.addVertex(id, nueva);
				}
				else if(idSalida!=-1)
				{
					grafoIntersecciones.addEdge(idSalida, idLlegada, peso);
				}
				
			}
		reader.endArray();
	}
	
}