/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author jdmen
 *
 */
public class RedBlackBST <Value, Key extends Comparable<Key> >
{
	private static final boolean RED = true;
	private static final boolean BLACK = false;

	private Node root;

	private Key min;

	private Key max;


	public RedBlackBST()
	{
		root= null;
	}
	private class Node
	{
		Key key; // key
		Value val; // associated data
		Node left, right; // subtrees
		int N; // # nodes in this subtree
		boolean color; // color of link from
		// parent to this node
		Node(Key key, Value val, int N, boolean color)
		{
			this.key = key;
			this.val = val;
			this.N = N;
			this.color = color;
		}
		public int size()
		{
			return N;
		}
	}
	private Value get(Node x, Key key)
	{ // Return value associated with key in the subtree rooted at x;
		// return null if key not present in subtree rooted at x.
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0) return get(x.left, key);
		else if (cmp > 0) return get(x.right, key);
		else return x.val;
	}

	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}

	private Node rotateLeft(Node h)
	{
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)
		+ size(h.right);
		return x;
	}

	private Node rotateRight(Node h)
	{
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)
		+ size(h.right);
		return x;
	}
	private Node put(Node h, Key key, Value pValue)
	{
		if(min == null && max == null)
		{
			min = key;
			max = key;
		}else
		{
			if(min.compareTo(key)>0)
			{
				min = key;
			}
			if(max.compareTo(key)<0)
			{
				max = key;
			}
		}

		if (h == null) // Do standard insert, with red link to parent.
		{
			return new Node(key, pValue, 1, RED);
		}
		int cmp = key.compareTo(h.key);
		if (cmp < 0) h.left = put(h.left, key, pValue);
		else if (cmp > 0) h.right = put(h.right, key, pValue);
		else h.val = pValue;
		if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right)) flipColors(h);
		h.N = size(h.left) + size(h.right) + 1;

		return h;
	}
	private int size(Node x)
	{
		if (x == null) return 0;
		else return x.N;
	}
	private boolean isRed(Node x)
	{
		if (x == null) return false;
		return x.color == RED;
	}

	   /**
     * Return the key in the symbol table whose rank is {@code k}.
     * This is the (k+1)st smallest key in the symbol table. 
     *
     * @param  k the order statistic
     * @return the key in the symbol table of rank {@code k}
     * @throws IllegalArgumentException unless {@code k} is between 0 and
     *        <em>n</em>�1
     */
    public Key select(int k) {
        if (k < 0 || k >= size()) {
            throw new IllegalArgumentException("argument to select() is invalid: " + k);
        }
        Node x = select(root, k);
        return x.key;
    }

    // the key of rank k in the subtree rooted at x
    private Node select(Node x, int k) {
        // assert x != null;
        // assert k >= 0 && k < size(x);
        int t = size(x.left); 
        if      (t > k) return select(x.left,  k); 
        else if (t < k) return select(x.right, k-t-1); 
        else            return x; 
    } 

    /**
     * Return the number of keys in the symbol table strictly less than {@code key}.
     * @param key the key
     * @return the number of keys in the symbol table strictly less than {@code key}
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public int rank(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to rank() is null");
        return rank(key, root);
    } 

    // number of keys less than key in the subtree rooted at x
    private int rank(Key key, Node x) {
        if (x == null) return 0; 
        int cmp = key.compareTo(x.key); 
        if      (cmp < 0) return rank(key, x.left); 
        else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
        else              return size(x.left); 
    } 
	public int size()
	{ 
		return size(root); 
	}

	// does this binary tree satisfy symmetric order?
		// Note: this test also ensures that data structure is a binary tree since order is strict
		private boolean isBST() {
			return isBST(root, null, null);
		}

		// is the tree rooted at x a BST with all keys strictly between min and max
		// (if min or max is null, treat as empty constraint)
		// Credit: Bob Dondero's elegant solution
		private boolean isBST(Node x, Key min, Key max) {
			if (x == null) return true;
			if (min != null && x.key.compareTo(min) <= 0) return false;
			if (max != null && x.key.compareTo(max) >= 0) return false;
			return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
		} 

		// are the size fields correct?
		private boolean isSizeConsistent() { return isSizeConsistent(root); }
		private boolean isSizeConsistent(Node x) {
			if (x == null) return true;
			if (x.size() != size(x.left) + size(x.right) + 1) return false;
			return isSizeConsistent(x.left) && isSizeConsistent(x.right);
		} 

		// check that ranks are consistent
		private boolean isRankConsistent() {
			for (int i = 0; i < size(); i++)
				if (i != rank(select(i))) return false;
			
			Iterator<Key> keysE = keys();
		    while (keysE.hasNext())
		    {
		    	Key key = keysE.next();
				if (key.compareTo(select(rank(key))) != 0) return false;
		    }
			return true;
		}

		// Does the tree have no red right links, and at most one (left)
		// red links in a row on any path?
		private boolean is23() { return is23(root); }
		private boolean is23(Node x) {
			if (x == null) return true;
			if (isRed(x.right)) return false;
			if (x != root && isRed(x) && isRed(x.left))
				return false;
			return is23(x.left) && is23(x.right);
		} 

		// do all paths from root to leaf have same number of black edges?
		private boolean isBalanced() { 
			int black = 0;     // number of black links on path from root to min
			Node x = root;
			while (x != null) {
				if (!isRed(x)) black++;
				x = x.left;
			}
			return isBalanced(root, black);
		}

		// does every path from the root to a leaf have the given number of black links?
		private boolean isBalanced(Node x, int black) {
			if (x == null) return black == 0;
			if (!isRed(x)) black--;
			return isBalanced(x.left, black) && isBalanced(x.right, black);
		}


	public boolean isEmpty()
	{
		return root == null;  	
	}

	public Value get(Key key)
	{
		return get(root, key); 
	}

	public int getHeight(Key key)
	{
		if(get(key)!= null) 
		{
			return getHeight(root, key);
		}
		else
		{
			return -1;
		}
	}
	private int getHeight(Node x, Key key)
	{
		int alturaActual = 0;

		int cmp = key.compareTo(x.key);

		if (cmp < 0) 
		{
			alturaActual = getHeight(x.left, key)+1;
		}
		else if (cmp > 0)
		{
			alturaActual = getHeight(x.right, key)+1;
		}
		else return 1;

		return alturaActual;
	}

	public boolean contains(Key pKey)
	{
		boolean contiene = false;

		if(get(pKey)!= null)
		{
			contiene = true;
		}

		return contiene;
	}
	public void put(Key key, Value val)
	{ // Search for key. Update value if found; grow table if new.
		root = put(root, key, val);
		root.color = BLACK;
	}
	public int height()
	{
		return profundidad(root);
	}


	public Key min()
	{
		return min;
	}

	public Key max()
	{
		return max;
	}

	public boolean check()
	{
		return isBST() && isSizeConsistent() && isRankConsistent() && is23() && isBalanced();
	}
	
	private int profundidad(Node node)  
	{ 
		if (node == null) 
			return 0; 
		else 
		{ 
			int arbolIzquierdo= profundidad(node.left); 
			int arbolDerecho = profundidad(node.right); 

			if (arbolIzquierdo > arbolDerecho) 
				return (arbolIzquierdo + 1); 
			else 
				return (arbolDerecho + 1); 
		} 
	}

	/**
	 * Returns all keys in the symbol table as an {@code Iterator}.
	 * To iterate over all of the keys in the symbol table named {@code st},
	 * use the foreach notation: {@code for (Key key : st.keys())}.
	 * @return all keys in the symbol table as an {@code Iterator}
	 */
	public Iterator<Key> keys() {
		if (isEmpty()) return new Queue<Key>().iterator();
		return keys(min(), max());
	}
	
	public Iterator<Value> valuesInRange (Key init, Key end)
	{
		return values(init,end);
	}
	
	public Iterator<Key> KeysInRange(Key init, Key end)
	{
		return keys(init, end);
	}

	/**
	 * Returns all keys in the symbol table in the given range,
	 * as an {@code Iterator}.
	 *
	 * @param  lo minimum endpoint
	 * @param  hi maximum endpoint
	 * @return all keys in the sybol table between {@code lo} 
	 *    (inclusive) and {@code hi} (inclusive) as an {@code Iterator}
	 * @throws IllegalArgumentException if either {@code lo} or {@code hi}
	 *    is {@code null}
	 */
	public Iterator<Key> keys(Key lo, Key hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<Key> queue = new Queue<Key>();
		// if (isEmpty() || lo.compareTo(hi) > 0) return queue;
		keys(root, queue, lo, hi);
		return queue.iterator();
	} 

	// add the keys between lo and hi in the subtree rooted at x
	// to the queue
	private void keys(Node x, Queue<Key> queue, Key lo, Key hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
		if (cmphi > 0) keys(x.right, queue, lo, hi); 
	}

	/**
	 * Returns all values in the symbol table in the given range,
	 * as an {@code Iterator}.
	 *
	 * @param  lo minimum endpoint
	 * @param  hi maximum endpoint
	 * @return all keys in the sybol table between {@code lo} 
	 *    (inclusive) and {@code hi} (inclusive) as an {@code Iterator}
	 * @throws IllegalArgumentException if either {@code lo} or {@code hi}
	 *    is {@code null}
	 */
	private Iterator<Value> values(Key lo, Key hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<Value> queue = new Queue<Value>();
		// if (isEmpty() || lo.compareTo(hi) > 0) return queue;
		values(root, queue, lo, hi);
		return queue.iterator();
	} 
	
	private void values(Node x, Queue<Value> queue, Key lo, Key hi)
	{
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) values(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.val); 
		if (cmphi > 0) values(x.right, queue, lo, hi); 
	}

	
}
