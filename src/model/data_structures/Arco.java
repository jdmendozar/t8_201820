package model.data_structures;

public class Arco <V,K extends Comparable<K>> {

	K idSalida;
	K idLlegada;
	double peso;
	
	public Arco(K nSal, K nLleg, double pPeso)
    {
    	peso = pPeso;
    	idSalida = nSal;
    	idLlegada = nLleg;
    }
	
	public double getPeso()
	{
		return peso;
	}
	
	public void setPeso(int p)
	{
		this.peso = p;
	}
	
	public K getidSalida()
	{
		return idSalida;
	}
	
	public K getidLlegada()
	{
		return idLlegada;
	}
	
	
}
