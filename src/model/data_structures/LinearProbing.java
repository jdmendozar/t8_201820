package model.data_structures;

import java.util.Iterator;

public class LinearProbing <V, K extends Comparable<K> >
{
	public NodoArco<K> primero;

	private double n;

	private int maxSize;

	private K[] keys;

	private V[] values;

	private double loadFactor;

	@SuppressWarnings("hiding")
	public class elIterator<K> implements Iterator<K>
	{
		private NodoArco<K> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIterator()
		{
			actual = (NodoArco<K>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public K next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darObjeto();
		}

	}


	public Iterator<K> iterator() {

		return new elIterator<K>();
	}

	@SuppressWarnings("unchecked")
	public LinearProbing(int initialValue, double loadF) 
	{

		if(isPrime(initialValue)== true)
		{	
			maxSize = initialValue;
		}
		else
		{
			maxSize = nextPrime(initialValue);	
		}
		n=0;

		keys = (K[]) new Comparable[maxSize];

		values = (V[]) new Object[maxSize];

		loadFactor = loadF;
	}

	public void put (K k,V v)
	{
		int pos = k.hashCode() % maxSize;

		if(n/maxSize >= loadFactor)
		{
			rehash();
		}
		
		if(n<maxSize)
		{

			if(keys[pos] == null)
			{
				keys[pos] = k;

				values[pos] = v;

				n++;
			}
			else
			{
				while(keys[pos] != null && pos<maxSize-1)
				{
					pos++;
				}

				if(pos == maxSize-1 && keys[pos] != null)
				{
					pos = 0;

					while(keys[pos]!= null)
					{
						pos++;
					}
				}

				keys[pos] = k;

				values[pos] = v;

				n++;

			}
		}

		if(n/maxSize >= loadFactor)
		{
			rehash();
		}
	}

	public V get (K k)
	{
		int posicion = k.hashCode() % maxSize;
		V resp= null;
		if(keys[posicion] == k)
		{
			resp = values[k.hashCode() % maxSize];
		}
		else
		{
			posicion++;
			boolean encontrado = false;

			while(posicion<maxSize && encontrado==false)
			{
				if(keys[posicion].equals(k))
				{
					encontrado = true;

					resp= values[posicion];
				}
				posicion++;
			}

			if(encontrado == false)
			{
				posicion = 0;

				while(posicion< k.hashCode() % maxSize && !encontrado)
				{
					if(keys[posicion].equals(k))
					{
						encontrado = true;

						resp= values[posicion];
					}
					posicion++;
				}
			}
		}
		return resp;
	}

	public V delete (K k)
	{
		int posOG = k.hashCode() % maxSize;

		K llave = keys[posOG];

		V v = null;

		if(llave != null)
		{
			if(llave== k)
			{
				v = values[posOG];

				keys[posOG] = null;

				values[posOG] = null;

				int posActual = posOG;

				boolean hayOtro =false;

				while(posActual < maxSize-1 && !hayOtro)
				{
					if(keys[posActual]!= null)
					{
						int posicionLlave = keys[posActual].hashCode() % maxSize;

						if(posicionLlave == posOG)
						{
							hayOtro = true;

							keys[posOG] = keys[posActual];

							keys[posActual] = null;

							values[posOG] = values[posActual];

							values[posActual] = null;
						}
					}

					posActual ++;
				}

				if(hayOtro== false)
				{
					posActual = 0;

					while(posActual < posOG && !hayOtro)
					{
						if(keys[posActual]!= null)
						{
							int posicionLlave = keys[posActual].hashCode() % maxSize;

							if(posicionLlave == posOG)
							{
								hayOtro = true;

								keys[posOG] = keys[posActual];

								keys[posActual] = null;

								values[posOG] = values[posActual];

								values[posActual] = null;
							}
						}

						posActual ++;

					}
				}
				n--;
			}
			else
			{
				int pos = posOG;

				boolean encontro= false;
				while(pos<maxSize && !encontro)
				{
					if(keys[pos]== k)
					{
						encontro = true;
					}
					pos++;
				}

				if(encontro == false)
				{
					pos = 0;
					
					while(pos<posOG && !encontro)
					{
						if(keys[pos]== k)
						{
							encontro = true;
						}
						pos++;
					}
				}
				
				if(encontro==true)
				{
					v = values[pos];

					keys[pos] = null;

					values[pos] = null;

					int posActual = pos;

					boolean hayOtro =false;

					while(posActual < maxSize-1 && !hayOtro)
					{
						if(keys[posActual]!= null)
						{
							int posicionLlave = keys[posActual].hashCode() % maxSize;

							if(posicionLlave == pos)
							{
								hayOtro = true;

								keys[pos] = keys[posActual];

								keys[posActual] = null;

								values[pos] = values[posActual];

								values[posActual] = null;
							}
						}

						posActual ++;
					}

					if(hayOtro== false)
					{
						posActual = 0;

						while(posActual < pos && !hayOtro)
						{
							if(keys[posActual]!= null)
							{
								int posicionLlave = keys[posActual].hashCode() % maxSize;

								if(posicionLlave == pos)
								{
									hayOtro = true;

									keys[pos] = keys[posActual];

									keys[posActual] = null;

									values[pos] = values[posActual];

									values[posActual] = null;
								}
							}

							posActual ++;

						}
					}
					n--;
				}
			}
		}
		return v;
	}

	/**
	 * 
	 */
	public void rehash ()
	{
		System.out.println("Entro al rehash");
		LinearProbing<V,K> t;

		t = new LinearProbing<V,K>(maxSize+1,loadFactor);

		for (int i = 0; i < maxSize; i++)
		{
			System.out.println("Veces que ha hecho el ciclo:  "+i);
			if (keys[i] != null)
			{
				t.put(keys[i], values[i]);
			}
		}

		keys = t.keys;

		values = t.values;

		maxSize = t.maxSize;
	}

	public double tamanoActual()
	{
		return n;
	}

	public int tamanoMax()
	{
		return maxSize;
	}

	/**
	 * Internal method to find a prime number at least as large as n.
	 * @param n the starting number (must be positive).
	 * @return a prime number larger than or equal to n.
	 */
	private static int nextPrime( int n )
	{
		if( n % 2 == 0 )
			n++;

		for( ; !isPrime( n ); n += 2 )
			;

		return n;
	}

	/**
	 * Internal method to test if a number is prime.
	 * Not an efficient algorithm.
	 * @param n the number to test.
	 * @return the result of the test.
	 */
	private static boolean isPrime( int n )
	{
		if( n == 2 || n == 3 )
			return true;

		if( n == 1 || n % 2 == 0 )
			return false;

		for( int i = 3; i * i <= n; i += 2 )
			if( n % i == 0 )
				return false;

		return true;
	}

	private void insertNode(NodoArco<K> aIns)
	{
		if(primero.darSiguiente()==null)
		{
			K nodoPrimero = primero.darObjeto();

			K comp = aIns.darObjeto();

			if(nodoPrimero.compareTo(comp)<=0)
			{
				primero.cambiarSiguiente(aIns);

				aIns.cambiarAnterior(primero);
			}else
			{
				primero.cambiarAnterior(aIns);

				aIns.cambiarSiguiente(primero);

				primero = aIns;
			}
		}
		else
		{
			NodoArco<K> actual = primero;

			boolean hecho = false;

			while(actual.darSiguiente()!= null && !hecho)
			{
				K nodoActual = actual.darObjeto();

				K comp = aIns.darObjeto();

				if(nodoActual.compareTo(comp)>0)
				{
					if(actual == primero)
					{
						aIns.cambiarSiguiente(primero);

						primero.cambiarAnterior(aIns);

						primero = aIns;
					}
					else
					{
						NodoArco<K> anterior = actual.darAnterior();

						actual.cambiarAnterior(aIns);

						aIns.cambiarAnterior(anterior);

						aIns.cambiarSiguiente(actual);

						anterior.cambiarSiguiente(aIns);
					}

					hecho = true;
				}

				actual = actual.darSiguiente();
			}

			K nodoActual = actual.darObjeto();

			K comp = aIns.darObjeto();

			if(actual.darSiguiente() == null)
			{
				if(nodoActual.compareTo(comp)<0)
				{
					actual.cambiarSiguiente(aIns);

					aIns.cambiarAnterior(actual);
				}else
				{
					NodoArco<K> anterior = actual.darAnterior();

					aIns.cambiarAnterior(anterior);

					aIns.cambiarSiguiente(actual);

					anterior.cambiarSiguiente(aIns);

					actual.cambiarAnterior(aIns);
				}
			}
		}
	}

	private void deleteNode(K k)
	{
		boolean hecho = false;
		if(primero.darSiguiente() == null)
		{
			if(primero.darObjeto().equals(k))
			{
				primero = null;
			}
		}else
		{
			NodoArco<K>actual = primero;

			while(actual.darSiguiente()!= null && !hecho)
			{
				if(actual.darObjeto().equals(k))
				{
					if(actual== primero)
					{
						NodoArco<K> siguiente = actual.darSiguiente();

						siguiente.cambiarAnterior(null);

						primero.cambiarSiguiente(null);

						primero = siguiente;
					}else
					{
						NodoArco<K>anterior = actual.darAnterior();

						NodoArco<K>siguiente = actual.darSiguiente();

						anterior.cambiarSiguiente(siguiente);

						if(siguiente != null)
						{
							siguiente.cambiarAnterior(anterior);
						}

						actual.cambiarAnterior(null);

						actual.cambiarSiguiente(null);
					}

					hecho  = true;
				}

				actual=actual.darSiguiente();
			}
		}
	}
}

