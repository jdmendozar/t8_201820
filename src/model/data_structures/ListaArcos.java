package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaHashGrafos.elIterator;

public class ListaArcos < K extends Comparable<K>>
{
	private NodoArco<K> primero;

	private int size;

	@SuppressWarnings("hiding")
	public class elIterator<K extends Comparable<K>> implements Iterator<K>
	{

		private NodoArco<K> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIterator()
		{
			actual = (NodoArco<K>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public K next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darKey();
		}

	}

	public Arco searchObject(K key)
	{
		NodoArco<K> buscado = null;

		NodoArco<K> nodoActual = primero;

		while(nodoActual != null && buscado == null)
		{
			if(nodoActual.darKey().equals(key))
			{
				buscado = nodoActual;
			}else
			{
				nodoActual = nodoActual.darSiguiente();
			}
		}

		return buscado.darValor();
	}

	public Iterator<K> iterator() {

		return new elIterator<K>();
	}

	public int size() {

		return size;
	}

	public Arco getPrimero() 
	{
		return primero.darValor();
	}
	
	public NodoArco getPrimeroNodo()
	{
		return primero;
	}

	public void add(K t, Arco a) 
	{
		NodoArco<K> actual = primero;
		if(primero == null)
		{
			primero = new NodoArco<K>(t, a);
			size++;
			return;
		}
		else
		{
			while(actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			NodoArco<K> b = new NodoArco<K>(t,a);
			b.cambiarAnterior(actual);
			actual.cambiarSiguiente(b);
			size++;
		}

	}


	public void addAtK(int pos, K e, Arco a) 
	{
		NodoArco<K> actual = primero;
		NodoArco<K> agregar = new NodoArco<K>(e,a);
		
		if(pos == 0)
		{
			primero = agregar;
			primero.cambiarSiguiente(actual);
			size++;
		}else if(pos == size)
		{
			add(e,a);
			return;
		}else if (pos < size)
		{
			int i =1;
			while (i <= pos)
			{
				actual = actual.darSiguiente();
				i++;
			}
			NodoArco<K> anterior = actual.darAnterior();
			agregar.cambiarAnterior(anterior);
			agregar.cambiarSiguiente(actual);
			anterior.cambiarSiguiente(agregar);
			actual.cambiarAnterior(agregar);
			size++;
		}
	}

	public K get(int pos) 
	{
		NodoArco<K> nodo = primero;

		for (int i = 1; i < pos; i++) 
		{
			nodo = nodo.darSiguiente();
		}
		return nodo.darKey();
	}

	public Arco remove(K t) 
	{
		NodoArco<K> aElimi = primero;
		if(primero.darKey().equals(aElimi.darKey()))
		{
			primero = aElimi.darSiguiente();
			aElimi.cambiarAnterior(null);
			size--;

		}else
		{
			boolean b = false;

			while(aElimi.darSiguiente() !=null && !b)
			{
				aElimi = aElimi.darSiguiente();
				b= aElimi.darKey().equals(aElimi.darKey())?true:false;
			}
			NodoArco<K> ant= aElimi.darAnterior();
			NodoArco<K> sig = aElimi.darSiguiente();
			ant.cambiarSiguiente(sig);
			if(sig != null)
			{
				sig.cambiarAnterior(ant);
			}
			size--;
		}

		return aElimi.darValor();
	}

	public void removeAtK(int pos) throws Exception 
	{
		NodoArco<K> actual = primero;
		if(pos >= size && pos <0)
		{
			throw new Exception("Posicion fuera de lista.");
		}
		if(pos == 0)
		{
			primero = primero.darSiguiente();
			primero.cambiarAnterior(null);
			size--;
		}else
		{
			for (int i = 1; i < pos; i++) {
				actual = actual.darSiguiente();
			}
			NodoArco<K> anterior = actual.darAnterior();
			if(actual.darSiguiente()!=null)
			{
				actual.darSiguiente().cambiarAnterior(anterior);
			}
			anterior.cambiarSiguiente(actual.darSiguiente());
			size--;
		}
	}



	public boolean isEmpty() {
		if(size==0)
			return false;
		else 
			return true;
	}

	public Arco get(K elem) {
		return searchObject(elem);
	}
	
	public class NodoArco <K extends Comparable<K>>
	{
		final K key;
		Arco valor;
		NodoArco<K> siguiente;
		NodoArco<K> anterior;

		public NodoArco(K k, Arco v)
		{
			key = k;
			valor = v;
		}

		public NodoArco<K> darAnterior() 
		{
			return anterior;
		}

		public void cambiarAnterior(NodoArco<K> actual) 
		{
			this.anterior = actual;
			
		}

		public NodoArco<K> darSiguiente()
		{
			return siguiente;
		}

		public void cambiarSiguiente( NodoArco<K> sig)
		{
			siguiente = sig;
		}

		public boolean tieneSiguienteEnCadena()
		{

			if(siguiente != null)
			{
				return true;
			}

			return false;
		}

		public K darKey()
		{
			return key;
		}

		public Arco darValor()
		{
			return valor;
		}

	}
	
}
