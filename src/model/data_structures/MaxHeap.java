package model.data_structures;

import java.util.ArrayList;

public class MaxHeap <T extends Comparable<T>> 
{
	private ArrayList<T> items;
	
	private int tamanoMaximo;
	
	private int tamanoActual;
	
	public MaxHeap (int pMax) 
	{
		items = new ArrayList<T>();
		
		tamanoMaximo = pMax;
		
		tamanoActual = 0;
	} 
	
	public int darNumElementos()
	{
		return tamanoActual;
	}
	
	public void agregar (T elemento) throws Exception
	{
		if(tamanoActual == tamanoMaximo)
		{
			throw new Exception("Ya se alcanz� el tama�o m�ximo de la cola");
		}
		tamanoActual++;
		items.set(items.size()-1, elemento);
		swim();

	}
	
	public void swim() {
		int k = items.size() -1;
		while(k>0)
		{
			int p = (k-1)/2;
			T hijo = items.get(k);
			T padre = items.get(p);
			
			if(hijo.compareTo(padre)>0)
			{
				items.set(k, padre);
				items.set(p, hijo);
				
				k=p;
			}
			else{
				break;
			}
		}
	}
	
	public void sink()
	{
		int k =0;
		int izq = 2*k+1;
		while( izq < items.size())
		{
			int max=izq;
			int der = izq +1;
			
			if(der < items.size())
			{
				if(items.get(der).compareTo(items.get(izq))>0)
				{
					max++;
				}
			}
			if(items.get(k).compareTo(items.get(max))<0)
			{
				T temp = items.get(k);
				items.set(k, items.get(max));
				items.set(max, temp);
				k = max;
				izq =2*k+1;
			}
			else
			{
				break;
			}
		}
	}
	 
	public T max() throws Exception
	{
		if(esVacia())
		{
			throw new Exception("No hay elementos en la cola");
		}
		else if(items.size()==1)
		{
			tamanoActual--;
			return items.remove(0);
		}
		else
			{
			T max = items.get(0);
			items.set(0, items.remove(items.size()-1));
			sink();
			return max;
			}
		
	}
	
	public boolean esVacia()
	{
		if(tamanoActual == 0)
		{
			return true;
		}else
		{
		return false;
		}
	}
	
	public int tamanoMax()
	{
		return tamanoMaximo;
	}
}
