package model.data_structures;


public class NodoHash<K,V> {
    final K key;
    V valor;
    NodoHash<K,V> siguiente;
    final int hash;

    public NodoHash(K k, V v, int h)
    {
    	key = k;
        valor = v;
        hash = h;      
    }
    
    

	public NodoHash<K,V> darSiguiente()
	{
		return siguiente;
	}
	
	public void cambiarSiguiente( NodoHash<K,V> sig)
	{
		siguiente = sig;
	}
	
	public boolean tieneSiguienteEnCadena()
	{
		
		if(siguiente != null)
		{
			return true;
		}
		
		return false;
	}
    
	public K darKey()
	{
		return key;
	}
	
	public V darValor()
	{
		return valor;
	}
}