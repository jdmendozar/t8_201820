package model.data_structures;

import java.util.Iterator;


public class Lista<T extends Comparable<T>>implements ILista<T>
{
	private NodoArco<T> primero;

	private int size;

	@SuppressWarnings("hiding")
	public class elIterator<T> implements Iterator<T>
	{

		private NodoArco<T> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIterator()
		{
			actual = (NodoArco<T>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public T next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darObjeto();
		}

	}

	public NodoArco<T> searchObject(T t)
	{
		NodoArco<T> buscado = null;

		NodoArco<T> nodoActual = primero;

		while(nodoActual != null && buscado == null)
		{
			if(nodoActual.darObjeto().equals(t))
			{
				buscado = nodoActual;
			}else
			{
				nodoActual = nodoActual.darSiguiente();
			}
		}

		return buscado;
	}

	public Iterator<T> iterator() {

		return new elIterator<T>();
	}

	public int size() {

		return size;
	}

	public T getFirst() 
	{
		return primero.darObjeto();
	}

	public void add(T t) 
	{
		NodoArco<T> actual = primero;
		if(primero == null)
		{
			primero = new NodoArco<T>(t);
			size++;
			return;
		}
		else
		{
			while(actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			NodoArco<T> a = new NodoArco<T>(t);
			a.cambiarAnterior(actual);
			actual.cambiarSiguiente(a);
			size++;
		}

	}


	public void addAtK(int pos, T e) 
	{
		NodoArco<T> actual = primero;
		NodoArco<T> agregar = new NodoArco<T>(e);
		if(pos == 0)
		{
			primero = agregar;
			primero.cambiarSiguiente(actual);
			size++;
		}else if(pos == size)
		{
			add(e);
			return;
		}else if (pos < size)
		{
			int i =1;
			while (i <= pos)
			{
				actual = actual.darSiguiente();
				i++;
			}
			NodoArco<T> anterior = actual.darAnterior();
			agregar.cambiarAnterior(anterior);
			agregar.cambiarSiguiente(actual);
			anterior.cambiarSiguiente(agregar);
			actual.cambiarAnterior(agregar);
			size++;
		}
	}

	public T get(int pos) 
	{
		NodoArco<T> nodo = primero;

		for (int i = 1; i <= pos; i++) 
		{
			nodo = nodo.darSiguiente();
		}
		return nodo.darObjeto();
	}

	public T remove(T t) 
	{
		NodoArco<T> aElimi = primero;
		if(primero.darObjeto().equals(aElimi))
		{
			primero = aElimi.darSiguiente();
			aElimi.cambiarAnterior(null);
			size--;

		}else
		{
			boolean b = false;

			while(aElimi.darSiguiente() !=null && !b)
			{
				aElimi = aElimi.darSiguiente();
				b= aElimi.darObjeto().equals(aElimi)?true:false;
			}
			NodoArco<T> ant= aElimi.darAnterior();
			NodoArco<T> sig = aElimi.darSiguiente();
			ant.cambiarSiguiente(sig);
			if(sig != null)
			{
				sig.cambiarAnterior(ant);
			}
			size--;
		}

		return aElimi.darObjeto();
	}

	public void removeAtK(int pos) throws Exception 
	{
		NodoArco<T> actual = primero;
		if(pos >= size && pos <0)
		{
			throw new Exception("Posicion fuera de lista.");
		}
		if(pos == 0)
		{
			primero = primero.darSiguiente();
			primero.cambiarAnterior(null);
			size--;
		}else
		{
			for (int i = 1; i < pos; i++) {
				actual = actual.darSiguiente();
			}
			NodoArco<T> anterior = actual.darAnterior();
			if(actual.darSiguiente()!=null)
			{
				actual.darSiguiente().cambiarAnterior(anterior);
			}
			anterior.cambiarSiguiente(actual.darSiguiente());
			size--;
		}
	}



	public boolean isEmpty() {
		if(size==0)
			return false;
		else 
			return true;
	}

	public T get(T elem) {
		return searchObject(elem).darObjeto();
	}


}
