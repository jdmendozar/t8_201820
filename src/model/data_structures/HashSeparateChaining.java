package model.data_structures;

import java.util.Iterator;



public class HashSeparateChaining <V,K extends Comparable<K>>
{
	private ListaHash<V, K>[] nodosTabla;
	private int tamanoActual;
	private int numElementos;
	public int factorCarga;
	
	
	

	@SuppressWarnings("unchecked")
	public HashSeparateChaining ( int m, int fCarga)
	{
		if(esPrimo(m)== true)
		{	
			tamanoActual = m;
		}
		else
		{
			tamanoActual = sigPrimo(m);	
		}
		
		nodosTabla = new ListaHash[tamanoActual];
		factorCarga = fCarga;
	}
	
	private int getPos(K pKey){
        int pos = pKey.hashCode() % nodosTabla.length;
        if(pos < 0)
        {
        	pos += nodosTabla.length;
        }
        return pos;
    }
	
	
	public void put(K pKey, V pValor)
	{
		int pos = getPos(pKey);
		
		if(nodosTabla[pos] == null)
		{
			nodosTabla[pos] = new ListaHash<>();
		}
		
		
        nodosTabla[pos].addAtK(0, pKey, pValor, pos);    
     
        numElementos++;
        
        
        if( numElementos/tamanoActual >= factorCarga )
        {
            tamanoActual = sigPrimo(2*tamanoActual);
        	rehash( tamanoActual);
        	
        }
	}
	
	
	public V get(K pKey)
	{
		int pos = getPos(pKey);
		V resp = null;
		
		for(NodoHash<K,V> node = nodosTabla[pos].getPrimero(); node != null; node = node.darSiguiente())
		{
            if((pos == node.hash) && pKey.equals(node.darKey()))
            {
            	resp = node.darValor();
            	return resp;
            }
		}
		
		return resp;
	}
	
	
	
	public V delete(K pKey)
	{
		int pos = getPos(pKey);
        V resp = null;
        
    	resp = nodosTabla[pos].remove(pKey);
    	numElementos--;

        return resp;
	}
	
	public ListaHash<V,K> valuesArrayAtN (int n)
	{
		return nodosTabla[n];
	}
	
	
	public void rehash(int pSize)
	{
		HashSeparateChaining<V,K> nueva = new HashSeparateChaining<V,K>(pSize, factorCarga);
        for(ListaHash<V, K> lista : nodosTabla)
        {
            
        	for(NodoHash<K, V> nodoActual = (NodoHash<K, V>) lista.getPrimero(); nodoActual != null; nodoActual = nodoActual.darSiguiente())
            {
                nueva.put(nodoActual.darKey(), nodoActual.darValor());
            }
        }
        nodosTabla = nueva.nodosTabla;
	}
	
	
	
	public int tamanoActual()
	{
		return tamanoActual;
	}
	

	/**
     * Internal method to find a prime number at least as large as n.
     * @param n the starting number (must be positive).
     * @return a prime number larger than or equal to n.
     */
    private static int sigPrimo( int n )
    {
        if( n % 2 == 0 )
            n++;

        for( ; !esPrimo( n ); n += 2 )
            ;

        return n;
    }

    /**
     * Internal method to test if a number is prime.
     * Not an efficient algorithm.
     * @param n the number to test.
     * @return the result of the test.
     */
    private static boolean esPrimo( int n )
    {
        if( n == 2 || n == 3 )
            return true;

        if( n == 1 || n % 2 == 0 )
            return false;

        for( int i = 3; i * i <= n; i += 2 )
            if( n % i == 0 )
                return false;

        return true;
    }

	
	
    public Iterator<K> keys() {

		return new elIterator<K>();
	}
    
    public Iterator<V> values()
    {
    	return new elIterator<V>();
    }
	
	public Iterator<V> valuesAtN (int n)
	{
		return nodosTabla[n].iteratorValues();
	}
	
	
    @SuppressWarnings("hiding")

	public class elIterator<K> implements Iterator<K>
	{
		private int pos;
		
		private int cont;
		
		boolean yaEntroLista;
		
		Iterator<K> iterActual;

		public elIterator()
		{
			pos = 0;
			
			cont = 0;
			
			yaEntroLista = false;
			
		}
		
		@Override
		public boolean hasNext() 
		{
		return cont<numElementos;
		}

		
		@SuppressWarnings("unchecked")
		@Override
		public K next() 
		{
			K aRetornar=null;

			while(nodosTabla[pos] == null && yaEntroLista == false)
			{
				pos++;
			}
			
			if(nodosTabla[pos] != null && yaEntroLista == false)
			{
				iterActual =  (Iterator<K>) nodosTabla[pos].iterator();
				yaEntroLista = true;
			}

			if(yaEntroLista == true)
			{
				if(iterActual.hasNext())
				{
					aRetornar = iterActual.next();
					cont++;
					
					if(iterActual.hasNext() == false)
					{
						yaEntroLista = false;
						pos++;
					}
				}
			}
			return aRetornar;
		}
	}
}
