package model.data_structures;

public class NodoArco<T> 
{
private NodoArco<T> siguiente;
	
	private NodoArco<T> anterior;

	private T object;

	public NodoArco (T ob)
	{
		object= ob;
	}

	public void cambiarSiguiente(NodoArco<T> sis)
	{
		siguiente = sis;
	}
	
	public void cambiarAnterior (NodoArco<T> ant)
	{
		anterior = ant;
	}

	public T darObjeto()
	{
		return object;
	}

	public NodoArco<T> darSiguiente()
	{
		return siguiente;
	}
	
	public NodoArco<T> darAnterior()
	{
		return anterior;
	}


}

