package model.data_structures;

import java.util.Iterator;

public class NodoVertice <K extends Comparable<K>,V>
{
	final K key;
	V valor;
	NodoVertice<K,V> siguiente;
	final int hash;
	private ListaArcos< K> arcos;

	public NodoVertice(K k, V v, int h)
	{
		key = k;
		valor = v;
		hash = h;   
		arcos = new ListaArcos<>();
	}

	public NodoVertice<K,V> darSiguiente()
	{
		return siguiente;
	}

	public void cambiarSiguiente( NodoVertice<K,V> sig)
	{
		siguiente = sig;
	}

	public boolean tieneSiguienteEnCadena()
	{

		if(siguiente != null)
		{
			return true;
		}

		return false;
	}

	public K darKey()
	{
		return key;
	}

	public V darValor()
	{
		return valor;
	}

	public Iterator<K> getKeysAdj()
	{
		Iterator<K> iterArcos = arcos.iterator();
		return iterArcos;
	}

	@SuppressWarnings("rawtypes")
	public ListaArcos getListaArcos()
	{
		return arcos;
	}

	@SuppressWarnings("rawtypes")
	public void agregarArco(K key, Arco pArco)
	{
		arcos.addAtK(0, key, pArco);
	}

}

