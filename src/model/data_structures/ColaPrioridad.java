package model.data_structures;


public class ColaPrioridad <T extends Comparable<T>>
{
	private int tamanoMaximo;

	private int tamanoActual;

	private Queue<T> colaPrioridad;

	public ColaPrioridad(int pMax) 
	{
		colaPrioridad = new Queue<T>();

		tamanoMaximo = pMax;

		tamanoActual = 0;
	}

	public int darNumElementos()
	{
		return tamanoActual;
	}

	public void agregar(T elemento)
	{
		boolean agregado = false;
		
		
		if(tamanoActual!= tamanoMaximo && colaPrioridad.isEmpty()==false)
		{

			if(tamanoActual!=0)
			{
				Queue<T> auxiliar = new Queue<T>();

				while(agregado == false)
				{
					T actual =  colaPrioridad.dequeue();

					if(actual.compareTo(elemento)<=0)
					{
						auxiliar.enqueue(elemento);

						auxiliar.enqueue(actual);

						tamanoActual ++;

					}
					else
					{
						auxiliar.enqueue(actual);
					}
				}

				if(colaPrioridad.isEmpty()== false)
				{
					while (!colaPrioridad.isEmpty())
					{
						T actual = colaPrioridad.dequeue();

						auxiliar.enqueue(actual);
					}
				}

				colaPrioridad = auxiliar;
			}
		}else 
		{
			colaPrioridad.enqueue(elemento);
			tamanoActual++;
		}
	}

	public T max()
	{
		return colaPrioridad.dequeue();

	}

	public boolean esVacia()
	{
		return tamanoActual==0;
	}

	public int tamanoMax()
	{
		return tamanoMaximo;
	}
}
