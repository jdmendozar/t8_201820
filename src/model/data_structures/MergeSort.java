package model.data_structures;

public class MergeSort<T> {
 
	@SuppressWarnings("rawtypes")
	public static void mergeSort(Comparable[] a)
	{
		sort(a, 0, a.length - 1);
	}
	
	@SuppressWarnings({ "rawtypes" })
	private static void merge(Comparable[] a, int indexInicio1, int indexFin1, int indexFin2) 
    { 
        int n1 = indexFin1 - indexInicio1 + 1; 
        int n2 = indexFin2 - indexFin1; 
        
        Comparable izq[] = new Comparable [n1]; 
        Comparable der[] = new Comparable [n2]; 
  
        for (int i=0; i<n1; ++i) 
            izq[i] = a[indexInicio1 + i]; 
        for (int j=0; j<n2; ++j) 
            der[j] = a[indexFin1 + 1+ j]; 
  
        int i = 0, j = 0; 
  

        int indexInicioMergedArray = indexInicio1; 
        while (i < n1 && j < n2) 
        { 
            if (compareTo(izq[i], der[j])) 
            { 
                a[indexInicioMergedArray] = izq[i]; 
                i++; 
            } 
            else
            { 
                a[indexInicioMergedArray] = der[j]; 
                j++; 
            } 
            indexInicioMergedArray++; 
        } 
  
        while (i < n1) 
        { 
            a[indexInicioMergedArray] = izq[i]; 
            i++; 
            indexInicioMergedArray++; 
        } 
  
        while (j < n2) 
        { 
            a[indexInicioMergedArray] = der[j]; 
            j++; 
            indexInicioMergedArray++; 
        } 
    } 
  

    @SuppressWarnings("rawtypes")
	private static void sort(Comparable[] a, int indexInicio, int indexFin) 
    { 
        if (indexInicio < indexFin) 
        { 
            int mitad = (indexInicio+indexFin)/2; 
            sort(a, indexInicio, mitad); 
            sort(a , mitad+1, indexFin); 
            merge(a, indexInicio, mitad, indexFin); 
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private static  boolean compareTo(Comparable t, Comparable g ) {
		if(t.compareTo(g)<0)
			return true;
		return false;
	} 
}
