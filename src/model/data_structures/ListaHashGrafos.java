package model.data_structures;

import java.util.Iterator;

public class ListaHashGrafos <V,K extends Comparable<K>>
{
	private NodoVertice<K, V> primero;

	private int size;

	public NodoVertice<K, V> searchObject(K t)
	{
		NodoVertice<K, V> buscado = null;

		NodoVertice<K, V> nodoActual = primero;

		while(nodoActual != null && buscado == null)
		{
			if(nodoActual.darKey().equals(t))
			{
				buscado = nodoActual;
			}else
			{
				nodoActual = nodoActual.darSiguiente();
			}
		}

		return buscado;
	}

	public Iterator<K> iterator() {

		return new elIterator<K>();
	}
	
	public Iterator<V> iteratorValues() {
		return new elIteratorValues<V>();
	}
	
	@SuppressWarnings("rawtypes")
	public Iterator<NodoVertice> iteratorNodes()
	{
		return new elIteratorNodes();
	}

	public int size() {

		return size;
	}

	public NodoVertice<K, V> getPrimero() 
	{
		return primero;
	}

	public void add(K pKey, V pValor, int h) 
	{
		NodoVertice<K, V> actual = primero;
		
		if(primero == null)
		{
			primero = new NodoVertice<K, V>(pKey,pValor, h);
			
			
			size++;
			
		}
		else
		{
			while(actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			NodoVertice<K, V> a = new NodoVertice<K, V>(pKey,pValor, h);
			actual.cambiarSiguiente(a);
			
			System.out.println("While");
			size++;
		}

	}


	public void addAtK(int pos, K pKey, V pValor, int h) 
	{
		NodoVertice<K, V> actual = primero;
		
		boolean bool = actual ==null;
		NodoVertice<K, V> agregar = new NodoVertice<K, V>(pKey,pValor, h);
		if(pos == 0)
		{
			primero = agregar;
			primero.cambiarSiguiente(actual);
			size++;
		}else if(pos == size)
		{
			add(pKey, pValor, h);
			return;
		}else if (pos < size)
		{
			int i =1;
			while (i <= pos)
			{
				actual = actual.darSiguiente();
				i++;
			}
			agregar.cambiarSiguiente(actual);

			size++;
		}
	}



	public V remove(K t) 
	{
		NodoVertice<K, V> aElimi = primero;
		if(primero.darKey().equals(t))
		{
			primero = primero.darSiguiente();
			size--;
		}else if(aElimi.darSiguiente() != null)
		{
			boolean elimino = false;

			while(aElimi.tieneSiguienteEnCadena() == true && !elimino)
			{
				
				NodoVertice<K, V> anterior = aElimi;
				aElimi = aElimi.darSiguiente();
				
				if(aElimi.darKey().equals(t))
				{

					anterior.cambiarSiguiente(aElimi.darSiguiente());
					elimino= true;
				}	
			}
			size--;
		}
		return aElimi.darValor();
	}


	public boolean isEmpty() {
		if(size==0)
			return false;
		else 
			return true;
	}
	
	
	
	@SuppressWarnings("hiding")
	public class elIterator<K extends Comparable<K>> implements Iterator<K>
	{

		private NodoVertice<K, V> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIterator()
		{
			actual = (NodoVertice<K, V>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public K next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darKey();
		}

	}
	
	@SuppressWarnings("hiding")
	public class elIteratorValues<V> implements Iterator<V>
	{

		private NodoVertice<K, V> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIteratorValues()
		{
			actual = (NodoVertice<K, V>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public V next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darValor();
		}

	}
	
	@SuppressWarnings({ "hiding", "rawtypes" })
	public class elIteratorNodes implements Iterator<NodoVertice>
	{

		private NodoVertice actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIteratorNodes()
		{
			actual = (NodoVertice<K, V>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public NodoVertice next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual;
		}

	}

}
