package model.data_structures;

import java.util.Iterator;

/*
 * author: @julian Mendoza
 */
public class Grafo <V,K extends Comparable<K>> 
{

	private int V;
	private int E;
	private HSCgrafo<V, K> hashGrafo;

	/**
	 * Initializes an empty graph with {@code V} vertices and 0 edges.
	 * param V the number of vertices
	 */
	public Grafo () 
	{
		this.V = 0; this.E = 0;
		
		hashGrafo = new HSCgrafo<>(1, 1);
	}
	
	public HSCgrafo<V, K> darGrafo ()
	{
		return hashGrafo;
	}
	
	
	public int V() {return V;}

	public int E() {return E;}

	public void addVertex( K idVertex, V infoVertex)
	{
		hashGrafo.put(idVertex, infoVertex);
		
		V++;
	}

	/**
	 * Adds the undirected edge v-w to this graph.
	 *
	 * @param  v one vertex in the edge
	 * @param  w the other vertex in the edge
	 * @throws IllegalArgumentException unless both {@code 0 <= v < V} and {@code 0 <= w < V}
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addEdge(K idVertexIni, K idVertexFin, double pPeso)
	{
		NodoVertice ini = hashGrafo.getNodo(idVertexIni); 
		
		NodoVertice fin = hashGrafo.getNodo(idVertexFin);
		
		Arco iniArco = new Arco<>(idVertexIni, idVertexFin, pPeso);
		
		Arco finArco = new Arco<>(idVertexFin, idVertexIni, pPeso);
		
		ini.agregarArco(idVertexFin, iniArco);
		
		fin.agregarArco(idVertexIni, finArco);
		
		E++;
	}

	public V getInfoVertex(K idVertex)
	{
		return hashGrafo.get(idVertex);
	}
	
	public void setInfoVertex(K idVertex, V infoVertex)
	{
		hashGrafo.modifyVertex(idVertex, infoVertex);
	}
	
	@SuppressWarnings("rawtypes")
	public NodoVertice getVertexNode(K pKey)
	{
		return hashGrafo.getNodo(pKey);
	}
	
	

	/**
	 * Returns the vertices adjacent to vertex {@code v}.
	 *
	 * @param  v the vertex
	 * @return the vertices adjacent to vertex {@code v}, as an iterable
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	@SuppressWarnings("unchecked")
	public Iterator<K> adj(K idVertex) 
	{
		Iterator<K> iter = hashGrafo.getNodo(idVertex).getKeysAdj();
		return iter;
	}

	/**
	 * Returns the degree of vertex {@code v}.
	 *
	 * @param  v the vertex
	 * @return the degree of vertex {@code v}
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int degree() 
	{
		return 0;
	}

}