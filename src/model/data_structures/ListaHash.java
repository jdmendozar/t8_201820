package model.data_structures;

import java.util.Iterator;


public class ListaHash <V,K extends Comparable<K>>
{
	private NodoHash<K, V> primero;

	private int size;


	public NodoHash<K, V> searchObject(K t)
	{
		NodoHash<K, V> buscado = null;

		NodoHash<K, V> nodoActual = primero;

		while(nodoActual != null && buscado == null)
		{
			if(nodoActual.darKey().equals(t))
			{
				buscado = nodoActual;
			}else
			{
				nodoActual = nodoActual.darSiguiente();
			}
		}

		return buscado;
	}

	public Iterator<K> iterator() {

		return new elIterator<K>();
	}
	
	public Iterator<V> iteratorValues() {
		return new elIteratorValues<V>();
	}

	public int size() {

		return size;
	}

	public NodoHash<K, V> getPrimero() 
	{
		return primero;
	}

	public void add(K pKey, V pValor, int h) 
	{
		NodoHash<K, V> actual = primero;
		
		if(primero == null)
		{
			primero = new NodoHash<K, V>(pKey,pValor, h);
			size++;
			return;
		}
		else
		{
			while(actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			NodoHash<K, V> a = new NodoHash<K, V>(pKey,pValor, h);
			actual.cambiarSiguiente(a);
			size++;
		}

	}


	public void addAtK(int pos, K pKey, V pValor, int h) 
	{
		NodoHash<K, V> actual = primero;
		NodoHash<K, V> agregar = new NodoHash<K, V>(pKey,pValor, h);
		if(pos == 0)
		{
			primero = agregar;
			primero.cambiarSiguiente(actual);
			size++;
		}else if(pos == size)
		{
			add(pKey, pValor, h);
			return;
		}else if (pos < size)
		{
			int i =1;
			while (i <= pos)
			{
				actual = actual.darSiguiente();
				i++;
			}
			agregar.cambiarSiguiente(actual);

			size++;
		}
	}



	public V remove(K t) 
	{
		NodoHash<K, V> aElimi = primero;
		if(primero.darKey().equals(t))
		{
			primero = primero.darSiguiente();
			size--;
		}else if(aElimi.darSiguiente() != null)
		{
			boolean elimino = false;

			while(aElimi.tieneSiguienteEnCadena() == true && !elimino)
			{
				
				NodoHash<K, V> anterior = aElimi;
				aElimi = aElimi.darSiguiente();
				
				if(aElimi.darKey().equals(t))
				{

					anterior.cambiarSiguiente(aElimi.darSiguiente());
					elimino= true;
				}	
			}
			size--;
		}
		return aElimi.darValor();
	}


	public boolean isEmpty() {
		if(size==0)
			return false;
		else 
			return true;
	}
	
	
	
	@SuppressWarnings("hiding")
	public class elIterator<K> implements Iterator<K>
	{

		private NodoHash<K, V> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIterator()
		{
			actual = (NodoHash<K, V>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public K next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darKey();
		}

	}
	
	@SuppressWarnings("hiding")
	public class elIteratorValues<V> implements Iterator<V>
	{

		private NodoHash<K, V> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIteratorValues()
		{
			actual = (NodoHash<K, V>) primero;
		}
		@Override
		public boolean hasNext() 
		{

			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public V next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darValor();
		}

	}

}
