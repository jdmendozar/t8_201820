package api;

import java.io.IOException;

import com.google.gson.stream.JsonReader;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */

	
	void loadStations (String pStationsFile);
	
	void loadIntersecciones(String pIntereseccionesFile);
	
	void loadArcosMallaVial(String pIntereseccionesFile);
	
	void escribirTotalesGrafoInicial (String pRuta);
	
	void loadStationsInterseccion();
	
	void escribirTotalesGrafoExtendido (String pRuta);
	
	void crearJson(String pRuta);
	
	void cargarJson(String tripsFile) throws IOException;

}
