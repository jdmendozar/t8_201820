package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Arco;
import model.data_structures.Grafo;
import model.data_structures.ListaArcos;
import model.data_structures.ListaArcos.NodoArco;
import model.data_structures.ListaHashGrafos;
import model.data_structures.NodoVertice;

public class GrafoTests {

	Grafo<Integer, Integer> testGrafo;

	private void setupScenario()
	{
		testGrafo = new Grafo<>();
	}

	@Test
	public void test() 
	{
		setupScenario();
		
		testGrafo.addVertex(1, 1);
		
		testGrafo.addVertex(2, 2);
		
		testGrafo.addVertex(3, 3);
		
		testGrafo.addEdge(1, 2, 1);
		
		testGrafo.addEdge(1, 3, 1);
		
		assertTrue(testGrafo.E() ==2);
		
		assertTrue(testGrafo.getVertexNode(2).getListaArcos().size() ==1);
		
		assertTrue(testGrafo.getVertexNode(3).getListaArcos().size() ==1);
		
		assertTrue(testGrafo.getVertexNode(1).getListaArcos().size() ==2);
		
		testGrafo.addVertex(4, 4);
		
		testGrafo.addVertex(5, 5);
		
		testGrafo.addEdge(5, 1, 2);
		
		testGrafo.addEdge(4, 2, 3);
		
		testGrafo.addEdge(5, 4, 2);
		
		testGrafo.addEdge(5, 3, 3);
		
		testGrafo.addEdge(4, 3, 4);
		
		assertTrue(testGrafo.getVertexNode(1).getListaArcos().size() ==3);
		
		assertTrue(testGrafo.getVertexNode(2).getListaArcos().size() ==2);
		
		assertTrue(testGrafo.getVertexNode(3).getListaArcos().size() ==3);
		
		assertTrue(testGrafo.getVertexNode(4).getListaArcos().size() ==3);

		assertTrue(testGrafo.getVertexNode(5).getListaArcos().size() ==3);
		
		assertTrue(testGrafo.E()==7);
		
		assertTrue(testGrafo.V()==5);
		
		
		assertTrue(testGrafo.getVertexNode(2).getListaArcos().getPrimeroNodo().darKey().equals(4));
		
		assertTrue(testGrafo.getVertexNode(2).getListaArcos().get(2).equals(1));
	}

}
