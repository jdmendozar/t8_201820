package Test;


import org.junit.Test;
import org.omg.CORBA.NO_IMPLEMENT;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;


import model.data_structures.RedBlackBST;


public class ArbolRojoNegroTests 
{
	RedBlackBST<String, Integer> tree;
	Integer[] keys;
	String[] values;

	private void setUp1()
	{
		tree = new RedBlackBST <String, Integer>();
		keys = new Integer [10];
		values = new String[10];


		tree.put(0, "A");

		tree.put(1, "B");
	}

	@Test
	public void test1()
	{
		setUp1();

		assertTrue("No se encuentra", tree.get(0) == "A");
		assertTrue("No se encuentra", tree.get(1)== "B");

		assertTrue(""+tree.getHeight(1), tree.getHeight(1)==1);
		assertTrue(tree.getHeight(0)== 2);
		assertTrue(tree.height()==2);

		tree.put(0,"C");
		assertTrue("No se cambio correctamente el valor", tree.get(0)== "C");

		tree.put(3, "val");
		tree.put(4, "val1");
		tree.put(5, "val2");
		tree.put(6, "val3");
		tree.put(7, "val4");
		tree.put(8, "val5");
		tree.put(9, "val6");
		tree.put(10, "val7");
		tree.put(11, "val8");
		tree.put(12, "val9");

		assertTrue(tree.size() == 12);

		assertTrue(tree.get(11)== "val8");
	}

	@Test
	public void test2()
	{
		setUp1();

		tree.put(3, "val");
		tree.put(4, "val1");
		tree.put(5, "val2");
		tree.put(6, "val3");
		tree.put(7, "val4");
		tree.put(8, "val5");
		tree.put(9, "val6");
		tree.put(10, "val7");
		tree.put(11, "val8");
		tree.put(12, "val9");

		assertTrue(tree.min()==0);
		assertTrue(tree.max()==12);

		assertTrue(tree.check()==true);

		Iterator<Integer> iter = tree.KeysInRange(3, 6);
		int cont = 3;
		while(iter.hasNext())
		{
			int actual = iter.next();

			assertTrue(actual== cont);

			cont++;
		}

		Iterator<String> valores = tree.valuesInRange(10, 12);

		String actual = valores.next();

		assertTrue(actual.equals("val7"));

		actual = valores.next();

		assertTrue(actual.equals("val8"));

		actual = valores.next();

		assertTrue(actual.equals("val9"));
	}
}